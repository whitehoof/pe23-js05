"use strict"
// ТЕОРІЯ:

/* 
1. Опишіть своїми словами, що таке метод об'єкту.

Це функція, яка знаходиться в обʼєкті.
*/



/*
2. Який тип даних може мати значення властивості об'єкта?

Будь-який.
*/



/*
3. Об'єкт це посилальний тип даних. Що означає це поняття?

Це означає, що змінна містить не самі дані обʼєкта, а посилання на них. Це впливає, зокрема, на результат копіювання обʼєкта через призначення (через оператор "=") — копіюються не самі дані, а посилання на них. Тому для створення дійсно незалежної копії даних, які містяться в обʼєкті, потрібно використовувати, наприклад, метод JSON:

const independentCopyOfData = JSON.parse(JSON.stringify(data));
*/






// ПРАКТИКА:


function createNewUser() {
	
	let firstFromPrompt = prompt("Введіть імʼя нового юзера:")
	let lastFromPrompt = prompt("Введіть прізвище нового юзера:")
	
	const newUser = {
		firstName: firstFromPrompt,
		lastName: lastFromPrompt,

		getLogin() {
			if(this.firstName == null || this.lastName == null) {
				throw new Error("\nCANNOT CREATE USER:\none of arguments is null.");
			} else {
				return (this.firstName.substring(0,1) + this.lastName).toLowerCase();
			}
		},
	};

	return newUser;
}

let user1 = createNewUser();
console.log(user1.getLogin()); // отримуємо в консоль логін юзера user1



// Оскільки prompt — це не завжди практично, 
// ось ще варіант реалізації задачі без запитування через промпт:
// функція не питає дані у юзера, а приймає дані від кодера:

console.log("Альтернативний метод без промпта:");

function createNewUserWithoutPrompt(firstName, lastName) {
	return {
		firstName,
		lastName,

		getLogin() {
			if(this.firstName == null || this.lastName == null) {
				throw new Error("One of arguments is null, cannot create user!");
			} else {
				return (this.firstName.substring(0,1) + this.lastName).toLowerCase();
			}
		},
	}
}

let user2 = createNewUserWithoutPrompt("Тарас", "Чмут");
console.log(user2.getLogin()); // отримуємо в консоль логін юзера user2